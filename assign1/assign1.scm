; author :  Sagar Ahire
; course : Essentials Of Programming Languages
; assignment 1 : Scheme Programming Exercise 

; 1a Procedure to add two numbers.
(define add (lambda (x y) (+ x y)))

; 1b Procedure to multiply two numbers.
(define mul (lambda (x y) (* x y)))

; 1c Type strict version of 1a.
(define addNum (lambda (x y) (if (and (number? x) (number? y)) (+ x y))))

; 1c Type strict version of 1b.
(define mulNum (lambda (x y) (if (and (number? x) (number? y)) (* x y))))

; 1d Display type of the datum recieved.
(define getType (lambda (x) (cond ((boolean?  x) 'boolean) ((symbol? x) 'symbol) ((char? x) 'char) ((vector? x) 'vector) ((procedure? x) 'procedure) ((pair? x) 'pair) ((number? x) 'number) ((string? x) 'string) ((port? x) 'port)))) 

; 1e Sum of the first 'N' natural numbers (recursive).
(define sumNR (lambda (x) (if (= x 1) x (+ x (sumNR (- x 1))))))

; 1e Sum of the first 'N' natural numbers (tail recursive).
(define sumNTR (lambda (x) (sumNNTR (- x 1) x)))
(define sumNNTR (lambda (x acc) (if (= x 1) (+ acc x) (sumNNTR (- x 1) (+ acc x))))) 

; 1f Product of the first 'N' natural numbers (tail recursive).
(define sumNTR (lambda (x) (sumNNTR (- x 1) x)))
(define sumNNTR (lambda (x acc) (if (= x 1) (* acc x) (sumNNTR (- x 1) (* acc x))))) 

; 1g Accumulation of the first 'N' natural numbers by given operator.
(define accum (lambda (x opr) (accumm (- x 1) x opr)))
(define accumm (lambda (x acc opr) (if (= x 1) (opr acc x) (accumm (- x 1) (opr acc x))))) 

; 1h1 Accumulation of natural numbers in a given range[a,b] by given operator.
(define accumRange (lambda (a b opr) (accumRange1 a (- b 1) b opr)))
(define accumRange1 (lambda (a b acc opr) (if (= a b) (opr acc b) (accumRange1 a (- b 1) (opr acc b) opr))))

; 3c Sum of first N numbers from the given list
(define sumNL (lambda (n lst) (if (= n 0) 0 (+ (car lst) (sumNL (- n 1) (cdr lst))))))

; 3d list of first N numbers from the given list
(define listNL (lambda (n lst) (if (= n 0) '() (cons (car lst) (listNL (- n 1) (cdr lst))))))

; 3e Is list has members of same type
(define sameMembers (lambda (lst) (if (= (length lst) 1)  #t (and (eqv? (getType (car lst)) (getType (car (cdr lst)))) (sameMembers (cdr lst))))))

; (define checkIt (lambda (tv lst) (and tv (eqv?  (
;(define mlength (lambda lst (mlen 0 lst)))
;(define mlen (lambda (n lst) (if (eqv? lst '()) n (mlen (+ n 1) (cdr lst)))))

(define genRL (lambda (n) (if (= n 0) '() (cons n (genRL (- n 1)))))) 

(define genL (lambda (n) (reverse (genRL n))))

;; map
;; <mmap> := <list> 
(define inc (lambda (x) (+ 1 x)))
(define my-map (lambda (pro lst) (if (null? lst) '() (cons (pro (car lst)) (my-map pro (cdr lst))))))


;; q4 a
;; gen procedure
;; gen :: Int -> [Int]
(define gen (lambda (n) (if (= n 0) '()
			    (cons n (gen (- n 1))))
		    ))

(define sumn (lambda (n gen) (if (null? n) n
				 (sumNL n (gen n)))))

;; q4 b
;; my-map :: (a->b) x [a] -> [b]
(define my-map (lambda (f lst) (if (null? lst) lst
				   (cons (f (car lst)) (my-map f (cdr lst)))

				   )))

;; q4 f
;; compose :: (a->b) x (c->a) -> b
(define compose (lambda (f g) (lambda (y) (f (g y)))))
;; compose3 :: (a->b) x (c->a) x (d->c) -> b
(define compose3 (lambda (f g h) (compose f (compose g h))))
