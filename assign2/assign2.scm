; author : Sagar Ahire
; EPL assignment 2
; Data : feb 5 2016

; question 1

; 1a define length of list recursively
; version 1 'explicit recursion'
; len [] = 0
; len lst = 1 + (cdr lst)

; version 2 'implicit recursion using map'
; len = sum `compose` (map (\x-> 1))

; version 3 'implicit recursion using fold'
; len = (fold (\x y-> x+1) acc) 

; helper procedures
(define foldl (lambda (opr acc lst)
	       (if (null? lst) acc
		   (foldl opr (opr acc (car lst)) (cdr lst)))))


(define sum (lambda (lst) (foldl + 0 lst)))

(define compose (lambda (f g) (lambda (arg) (f (g arg)))))

(define my-map (lambda (f lst) (if (null? lst) lst
				   (cons (f (car lst)) (my-map f (cdr lst))))))

					    
; 1b length of list procedure
(define my-lengthv1 (lambda (lst) 
		      (if (null? lst) 0 
			  (+ 1 (my-lengthv1 (cdr lst))))))

(define my-lengthv2 (lambda (lst)
		      (sum (my-map (lambda (x) '1) lst))))


(define my-lengthv3 (lambda (lst)
		      (foldl (lambda (x y) (+ x 1)) 0 lst)))


; 1c append lists l1 & l2
(define my-appendv1 (lambda (l1 l2) (if (null? l1) l2
				      (cons (car l1) (my-appendv1 (cdr l1) l2)))))

; 1d flatten list
(define flatten (lambda (lst) (if (null? lst) lst
				  (if (list? (car lst)) (my-appendv1 (flatten (car lst)) (flatten (cdr lst)))
				      (cons (car lst) (flatten (cdr lst)))))))

; 1e reverse list
(define my-reverse (lambda (lst) (foldl (lambda (x y) (cons y x)) '() lst)))

; 1f list index of first occurence
; (define list-index (lambda (s lst) (foldl (lambda (a y) (if (car (cdr a)) (if (eqv? s y) ((+ (car a) 1). #f) ((+ (car a) 1) . #t)) a)) (0 #t) lst)))
(define list-index (lambda (s lst) (if (null? lst) -1 (if (eqv? s (car lst)) 0 (+ 1 (list-index s (cdr lst)))))))								
; 1g list indices of all occurences
; (define list-index (lambda (s lst) (foldl (lambda (p x) (if (eqv? s x) ((+ (car p) 1) (cons (car p) (cdr p))) ((+ (car p) 1) p))) (0 . '('-1.())) lst))) 							    

; 1h invert list of pairs
(define invert (lambda (lst) (my-map reverse lst)))
 				     
; testing procedures
(define lst '(1 2 3 4))
(define lst2 '(1 ((2 3) 3) 4))
(define 2lst '((1 2) (3 4)))
(define tmap (my-map (lambda (x) (+ x 1)) lst))
(define tfoldl (foldl + 0 lst))
(define tsum (sum lst))
(define testl1 (my-lengthv1 lst)) 		  
(define testl2 (my-lengthv2 lst))	       
(define testl3 (my-lengthv3 lst)) 
(define testapnd (my-appendv1 lst lst))
(define testflt (flatten lst2))
(define testrev (my-reverse lst))
(define testli (list-index 4 lst)) 
(define testinv (invert 2lst))


; 2. Inductive Specificaion

; a. Syntactic derivation to prove (-7 .(3 .(14 . ()))) is a list of numbers.
; by defination 
; <numlist> = '() | (num <numlist>)
;given l = (-7 .(3 .(14 . '()))))
;        = (num (3 . (14 . '())))      <- -7 isNUM
;        = (num (num (14 . '())))      <- 3 isNUM
;        = (num (num (num <numlist>))) <- 14 is NUM and '() is numlist from defination of numlist
;        = (num (num <numlist>))       <- replacing (num <numlist>) with <numlist> by defination
;        = (num <numlist>)
;        = <numlist>

;; Inductive Specifications of list of types

;; 2b. list of numbers
;; <numlist> = '() | (num <numlist>)

;; 2c. list of characters
;; <charlist> = '() | (char <charlist>)

;; 2d. list of booleans
;; <boollist> = '() | (bool <boollist>)

;; 2e. list of strings
;; <stringlist> = '() | (string <stringlist>)

;; 2f. list of symbols
;; <symlist> = '() | (sym <symlist>)

;; 2g. nesting of list
;; 2gb. nested list of numbers
;; <nestnumlist> = <numlist> | (<numlist> <nestnumlist>)
;; 2gc. nested list of chars
;; <nestcharlist> = <charlist> | (<charlist> <nestedcharlist>)
;; similary we can define 2gd to 2gf

;; 2i. binary tree
;; <bintree> ::= num | (symbol <bintree> <bintree>)
;; above PBNF cannot capture the context of binary search trees

;; 3 Recursively Specified Programs
;; 3a.  Addition of two natural numbers
;;
(define succ (lambda (x) (+ 1 x)))
(define pred (lambda (x) (- x 1)))
(define add (lambda (x y) (if (= x 0) y (add (pred x) (succ y)))))
(define sub (lambda (x y) (if (= y 0) x (sub (pred x) (pred y)))))
(define prod (lambda (x y) (prodd x y 0)))
(define prodd (lambda (x y p) (if (= y 0) p (prodd x (pred y) (add p x)))))
(define expo (lambda (x y) (expoo x y 1)))
(define expoo (lambda (x y e) (if (= y 0) e (expoo x (pred y) (prod e x)))))

;; <btree> := (num) | (num <btree> <btree>)
(define btree '(5 (4 (1) (2)) (3 (6) (7))))
(define bt '(5 (4) (3)))
(define bt2 '(10 (5 (4 (1) (2)) (3 (6) (7))) (5 (4) (3))))

(define preorder (lambda (bt) (if (or (null? bt) (= (length bt) '1)) bt 
				 (cons (car bt) (my-appendv1 (preorder (car (cdr bt))) (preorder (car (cdr (cdr bt)))))))))

 
(define inorder (lambda (bt) (if (or (null? bt) (= (length bt) '1)) bt
				 (my-appendv1 (inorder (car (cdr bt))) (cons (car bt) (inorder (car (cdr (cdr bt))))))))) 

(define postorder (lambda (bt) (if (or (null? bt) (= (length bt) '1)) bt
				   (my-appendv1 (postorder (car (cdr bt))) (my-appendv1 (postorder (car (cdr (cdr bt)))) (list (car bt))))))) 


(define nthelement (lambda (lst n) (if (null? lst) lst
				       (if (= n 0) (car lst)
					   (nthelement (cdr lst) (- n 1))))))

(define list-of-numbers? (lambda (lst) (if (null? lst) '#t
					   (and (num? (car lst)) (list-of-numbers? (cdr lst))))))

(define remove-first (lambda (lst s) (if (null? lst) lst
					 (if (eqv? (car lst) s) (cdr lst)
					     (cons (car lst) (remove-first (cdr lst)))))))

(define remove-all (lambda (lst s) (if (null? lst) lst
				       (if (eqv? (car lst) s) (remove-all (cdr lst))
					   (cons (car lst) (remove-all (cdr lst)))))))
;;<symlist> := '() | (sym <symlist>)
;;<slists> := <symlist> | (<symlist> <slists>)
(define sl '(d (f g h) (k (k) l)))
(define substitute (lambda (old new sl) (if (null? sl) sl
					    (if (list? (car sl)) (cons (substitute old new (car sl)) (substitute old new (cdr sl)))
						(if (eqv? (car sl) old) (cons new (substitute old new (cdr sl)))
						    (cons (car sl) (substitute old new (cdr sl))))))))
						    
						
					   
