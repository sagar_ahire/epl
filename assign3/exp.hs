data Btree a = Leaf a | Branch a (Btree a) (Btree a) deriving(Show)		

bt :: Btree Int
bt = Branch 3 (Branch 4 (Leaf 5) (Leaf 6)) (Branch 0 (Leaf 8) (Leaf 4))


--foldlt :: (a -> Btree Int -> a) -> a -> Btree Int -> a
--foldlt f a (Leaf b) = f a (Leaf b)
--foldlt f a (Branch b bt1 bt2) = f a b   


sumtree :: Btree Int -> Int
sumtree bt = f 0 bt
	where
		f x (Branch a bt1 bt2) = (x + a) + (sumtree bt1) + (sumtree bt2)
		f x (Leaf a) = x + a