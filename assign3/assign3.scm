;;author: sagar ahire
;;EPL : Data Abstraction Excersice 
;;assignment : 3

;;Implementaion of natural number system

;;Unary representation

;; ctors (constuctors)
;;1. zero
(define zerou '())
;;2. successor
(define successoru (lambda (n) (cons '#t n)))
;;3 . predecessoru
(define predecessoru (lambda (n) (if (null? n) n
				    (cdr n))))
;; Predicate
;; 1. is-zero?
(define is-zerou? (lambda (n) (null? n)))

;; n1
(define n1 '(#t #t #t))
(define n2 '(#t #t #t #t))
;; Addition
(define add1 (lambda (n1 n2) (if (is-zerou? n2) n1
				 (add1 (successoru n1) (predecessoru n2)))))
;; Substract
(define sub1 (lambda (n1 n2) (if (is-zerou? n2) n1
				 (sub1 (predecessoru n1) (predecessoru n2)))))
;; Multiplication
(define multt1 (lambda (n1 n2 n3) (if (is-zerou? n2) n3
				      (multt1 n1 (predecessoru n2) (add1 n1 n3)))))

(define mult1 (lambda (n1 n2) (if (or (is-zerou? n2) (is-zerou? n1)) zerou
				  (multt1 n1 n2 zerou))))

;; factorial using above number ctor and pred
(define fact1 (lambda (n) (if (is-zerou? n) (successoru zerou)
			      (mult1 n (fact1 (predecessoru n))))))



;;Scheme number representation

;; ctors
;;1.zero
(define zeros '0)
(define successors (lambda (n) (+ n 1)))
(define predecessors (lambda (n) (- n 1)))
;; pred
(define is-zeros? (lambda (n) (= n '0)))

(define add2 (lambda (n1 n2) (if (is-zeros? n2) n1
				 (add2 (successors n1) (predecessors n2)))))

(define sub2 (lambda (n1 n2) (if (is-zeros? n2) n1
				 (sub2 (predecessors n1) (predecessors n2)))))

(define multt2 (lambda (n1 n2 n3) (if (is-zeros? n2) n3
				      (multt2 n1 (predecessors n2) (add2 n1 n3)))))				      

(define mult2 (lambda (n1 n2) (if (or (is-zeros? n2) (is-zeros? n1)) zeros
				  (multt2 n1 n2 zeros))))

(define fact2 (lambda (n) (if (is-zeros? n) (successors zeros)
			       (mult2 n (fact2 (predecessors n))))))
;;Bignum representation
				  
;; ctors
;; 1. Zero
(define zerob '())
(define base 8)
;; 2. Successor
(define successorb (lambda (n) (if (null? n) '(1)
				   (if (< (car n) (- base 1)) (cons (+ (car n) 1) (cdr n))
				       (cons '0 (successorb (cdr n)))))))
;; 3. predecessorb
(define predecessorb (lambda (n) (if (null? n) n
				   (if (> (car n) '0) (cons (- (car n) 1) (cdr n))
				       (cons '0 (predecessorb (cdr n)))))))
;; pred
;; isZero?
(define is-zerob? (lambda (n) (if (null? n) '#t
				(and (= (car n) '0) (is-zerob? (cdr n))))))


(define add3 (lambda (n1 n2) (if (is-zerob? n2) n1
				 (add3 (successorb n1) (predecessorb n2)))))

(define sub3 (lambda (n1 n2) (if (is-zerob? n2) n1
				 (sub3 (predecessorb n1) (predecessorb n2)))))

(define multt3 (lambda (n1 n2 n3) (if (is-zerob? n2) n3
				      (multt3 n1 (predecessorb n2) (add3 n3)))))				      

(define mult3 (lambda (n1 n2) (if (or (is-zerob? n2) (is-zerob? n1)) zerob
				  (multt3 n1 n2 zerob))))

(define fact3 (lambda (n) (if (is-zerob? n) (successorb zerob)
			       (mult3 n (fact3 (predecessorb n))))))


;; general helper procedures
;; fold :: (a -> b -> c) -> a -> [b] -> c
(define fold (lambda (pro acc list) (if (null? list) acc
					(fold pro (pro acc (car list)) (cdr list)))))

;; q1. Substitute list of old symbols by list of new symbols in slist
;; substitute :: [symbols] x [symbols] x Exp -> Exp
(define substitute ((lambda (sub) (lambda (oldsyms newsyms slist) (if (or (null? oldsyms) (null? newsyms)) slist
								      (substitute (cdr oldsyms) (cdr newsyms)
										  (map (lambda (e) (sub (car oldsyms) (car newsyms) e))
										       slist)))))
		    (lambda (os ns exp) (if (null? exp) exp
				    (if (list? exp) (map (lambda (e) (sub os ns e)) exp)
					(if (eqv? os exp) ns
					    exp)))))) 

;;input exp
(define exp '(define fun (lambda (x y z ) (if x 1 (z 0 y)))))

;;q2 split-two-list
;;split-two-list :: [(a,b)] -> [[a],[b]]
(define split-two-list ((lambda (f g) (lambda (lst) (if (null? lst) lst
							(g (map reverse (fold f '(() ()) lst))))))
			(lambda (x y) (cons (cons (car y) (car x)) (list (cons (car (cdr y)) (car (cdr x))))))
			(lambda (lst) (cons (car lst) (car (cdr lst)))))) 

;; input two list
(define tl '((a 1) (b 2) (c 3)))

;;q3 desugar-let
;;auxillary procedure
;;tlet :: slist -> slist
(define tlet (lambda (lst) (if (or (null? lst) (not (list? lst))) lst
			       (if (not (eqv? (car lst) 'let)) (map tlet lst)
				   ((lambda (sptl) (let ((ebody (car (cdr (cdr lst)))))
						     (cons (cons 'lambda (cons (car sptl) (list (map tlet ebody)))) (cdr sptl))))
				    (split-two-list (car (cdr lst))))))))

;;desugar-let :: exp -> exp
(define desugar-let (lambda (exp) (map tlet exp)))
;; sample input 
(define inputexp '(define remove-first
	       (lambda (sym list-of-syms)
		 (if (null? list-of-syms)
		     list-of-syms
		     (let ((a (car list-of-syms))
			   (b (cdr list-of-syms)))
		       (if (eq? sym a)
			   b
			   (cons a
				 (remove-first sym b))
			   ))))))

;;execute
;;(desugar-let inputexp)

;;sample output
(define outputexp '(define remove-first
		(lambda (sym list-of-syms)
		  (if (null? list-of-syms)
		      list-of-syms
		      ((lambda (a b)
			 (if (eq? sym a)
			     b
			     (cons a (remove-first sym b)))
			 )
		       (car list-of-syms)
		       (cdr list-of-syms))
		      )
		  ))

)



;;q3 Count occurences of given symbol in given slist
;;helper procedure
;;foldtree :: symbol x ((symbol x symbol) -> bool) x (int x int -> int) x int x slist -> int
(define foldtree (lambda (pred proc acc tree) (if (null? tree) acc
					     (if (list? (car tree) ) (foldtree pred proc (proc acc (foldtree pred proc 0 (car tree))) (cdr tree))
						 (foldtree pred proc (proc acc (pred (car tree))) (cdr tree))
					     )
			 ))
)

(define one (lambda (x) (lambda (y) (if (eqv? x y) 1
					0))))

;; version 1 (using modified fold)
(define count-occurrences1 (lambda (x slst) (foldtree (one x) + 0 slst)))

;; version 2 (using general fold , making it consise)
(define count-occurrences2 (lambda (x slst) ( (co x) 0 slst)))
(define co (lambda (x) (lambda (count btree) (if (null? btree) count
						 (if (list? btree) (+ count (fold (co x) 0 btree))
						     (if (eqv? x btree) (+ count 1)
							 count))))))
								       
;;input
(define sl '(s (d (d f) (f g (g g h (g h)) ))))

;; execute
;; (count-occurrences1 'g sl)
;; (count-occurrences2 'g sl)

;;q5 path procedure to find path to an element in BST
;; path :: int x bst -> [symbol]
(define path (lambda (n bst) (if (or (null? bst) (= n (car bst))) '()
				 (if (< n (car bst)) (cons 'Left (path n (car (cdr bst))))
				     (cons 'Right (path n (car (cdr (cdr bst)))))
				     ))))

(define btree '(14 (7 () (12 () ()))
		   (26 (20 (17 () ())
			   ())
		       (31 () ()))))

;; experiments
;; sumbtree :: slist -> int
(define sumbtree (lambda (x btree) (if (null? btree) x
				       (if (list? btree) (+ x (fold sumbtree 0 btree))
					   (+ x btree)))))
					   
					 
;; q6 free-variable
;; lambda-term grammar
;; lexp := identifier | (lexp lexp) | (lambda (identifier) lexp)				 
;; lambda-term defination in rules-of-inference style

;; 1.    x belongs to identifier
;;    _____________________________________________
;;       x belongs to lambda-expression

;; 2.  (l1 belongs to lambda-expression) (l2 belongs to lambda-expression)
;;    _______________________________________________________________________
;;        (l1 l2) belongs to lambda-expression

;;3.  (args belongs to list-of-an-identifier) (l belongs to lambda-expression)
;;  ___________________________________________________________________________
;;    (lambda args l) belongs to lambda-expression


;;q6
;; defination of free variable
;; A variable x occurs free in exp E iff
;; 1. E is an identifier and E is same as x

;; (E belongs-to lambda-term identifier) (=E x)
;; _____________________________________________
;;  x is free in E

;; 2. E is an Application (E1 E2) and x is free in E1 or E2

;; (E is lambda-term Application (E1 E2)) (x is free in E1 or E2)
;; ________________________________________________________________
;;          x is free in E

;; 3. E is an Abstraction (lambda (y) E1) , y is different from x and x occurs free in E1
;; (E is lambda-term Abstraction (Lambda (y) E1)) (x not-equal y) (x occurs free in E1)
;; __________________________________________________________________________________________
;;     x is free in E




;;free? :: Sym x Lexp -> Bool
(define free? (lambda (sym lexp) (if (null? lexp) '#f
				     (if (not (list? lexp)) (eqv? sym lexp)
					 (if (eqv? 'lambda (car lexp)) (and (not (eqv? sym (car (car (cdr lexp))))) (free? sym (car (cdr (cdr lexp)))))
					     (or (free? sym (car lexp)) (free? sym (car (cdr lexp))))
					     )))))


;; q7
;; defination of bound variable
;; A variable x is bound in E iff
;; 1.
;;   (E is of the form (E1 E2))  ( x occurs bound in E1 or E2)
;; ____________________________________________________________
;;     x is bound in E

;; 2.
;;  (E is of the form (lambda (y) E1)) (x occurs bound in E1 or (x and y are same and x occurs free in E1))
;; __________________________________________________________________________________________________________
;;  x is bound in E

;;bound? :: Sym x Lexp -> Bool
(define bound? (lambda (sym lexp) (if (null? lexp) '#f
				      (if (not (list? lexp)) '#f
					  (let ((cl (car lexp)))
					    (if (eqv? 'lambda cl) (let ((c1 (car (cdr (cdr lexp)))) (c2 (car (car (cdr lexp)))))
								    (or (and (eqv? sym c2) (free? sym c1))
								    (bound? sym c1)))
					      (or (bound? sym cl) (bound? sym (car (cdr lexp))))
					      ))))))


(define t1 (bound? 'z '(lambda (y) (lambda (z) (x (y z))))))
(define t2 (free? 'z '(lambda (y) (lambda (z) (x (y z))))))
(define t33  (free? 'x '( (lambda (y) (y x)) (lambda (x) (x x)))))
(define t4  (bound? 'x '( (lambda (y) (y x)) (lambda (x) (x x)))))

;;9 bount-set
;;bound-set :: Lexp -> {sym}
(define bound-set (lambda (lexp) (if (or (null? lexp) (not (list? lexp))) '()
				     (if (eqv? 'lambda (car lexp))
					 (let ((sym (car (car (cdr lexp)))) (le (car (cdr (cdr lexp))))) 
					 (if (free? sym le) (cons sym (bound-set le))
					     (bound-set le)))
					 
					 (append (bound-set (car lexp)) (bound-set (car (cdr lexp)))))
				     )))


(define t3 (bound-set '(lambda (y) (lambda (y) (lambda (z) (x (y z b)))))))

;;10. free-set
;;free-set :: Lexp -> {sym}

							
