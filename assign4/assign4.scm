;; author : sagar ahire
;; rollno: 13102
;; EPL
;; Assignment 4 : A Simple Interpreter for a simplified Lambda Calculus


;;following is the grammar of Lambda Calculus
;; <Expression> := <var-ref>
;;              |<number>
;;              |(lambda ({<var-ref>}*) <Expression>)
;;              |(<Expression> <Expression>)
;;              |(<num-op> <Expression> <Expression>)



;;<num-op> := '+' | '*'
;;<number> := {0|1|2|3|4|5|6|7|8|9}*


;;generating AST
  
;; parse :: Lexp -> Last
(define parse (lambda (exp) (cond
			     ((null? exp) exp)
			     ((symbol? exp) `(Var ,exp))
			     ((number? exp) `(Num ,exp))
			     ((list? exp)
			      (cond
			       ((eqv? (car exp) 'lambda) `(Abs ,(car (cadr exp)) ,(parse (caddr exp))))
			       ((eqv? (length exp) 2) `(App ,(parse (car exp)) ,(parse (cadr exp))))
			       ((and (= (length exp) 3) (or (eqv? (car exp) '*) (eqv? (car exp) '+))) `(Opr ,(car exp) ,(parse (cadr exp)) ,(parse (caddr exp))))
			       (else 'InvalidLExp))
			     ))))


;; basic-lookup :: Sym X ((Sym Lexp)) -> Lexp
(define basic-lookup (lambda (var env) (if (null? env) var
					   (if (eqv? var (car (car env))) (car (cdr (car env)))
					       (basic-lookup var (cdr env))))))

;; lookup :: Last X ((Sym Lexp)) -> Last
(define lookup (lambda (last env) (cond
				   ((or (null? last) (eqv? last 'InvalidLExp)) last)
				   ((eqv? (car last) 'Var) (let ((sexp (basic-lookup (cadr last) env)))
							     (if (list? sexp) (lookup (parse sexp) env)
								 (parse sexp))))
				    	  
				   ((eqv? (car last) 'App)  `(App ,(lookup (cadr last) env) ,(lookup (caddr last) env)))
				   ((eqv? (car last) 'Abs)  `(Abs ,(cadr last) ,(lookup (caddr last) env)))
				   ((eqv? (car last) 'Opr)  `(Opr ,(cadr last) ,(lookup (caddr last) env) ,(lookup (cadddr last) env)))
				   (else last))
		       ))
		       
				    

			     
;; rename :: Last X Sym X Sym -> Last
(define rename (lambda (last sym1 sym2) (cond
					 ((or (null? last) (eqv? last 'InvalidLExp)) last)
					 ((and (eqv? (car last) 'Var) (eqv? sym1 (cadr last))) `(Bound-Var ,sym2))
					 ((eqv? (car last) 'App) `(App ,(rename (cadr last) sym1 sym2) ,(rename (caddr last) sym1 sym2)))
					 ((eqv? (car last) 'Abs) `(Abs ,(cadr last) ,(rename (caddr last) sym1 sym2)))
					 ((eqv? (car last) 'Opr) `(Opr ,(cadr last) ,(rename (caddr last) sym1 sym2) ,(rename (cadddr last) sym1 sym2)))
					 (else last))))

;;alpha-convert :: Last -> Last
(define alpha-convert (lambda (last) (alpha-conv last 0)))
;;alpha-conv :: Last -> Int -> Last
(define alpha-conv (lambda (last level) (cond
					 ((or (null? last) (eqv? last 'InvalidLExp)) last)
					 ((eqv? (car last) 'Abs) `(Abs ,level  ,(rename (alpha-conv (caddr last) (+ level 1)) (cadr last) level)))
					 ((eqv? (car last) 'App) `(App ,(alpha-conv (cadr last) level) ,(alpha-conv (caddr last) level)))
					 ((eqv? (car last) 'Opr) `(Opr ,(cadr last) ,(alpha-conv (caddr last) level) ,(alpha-conv (cadddr last) level)))
					 (else last)
					 )))


;;substitute :: Sym X Sym X Last -> Maybe Last
(define substitute (lambda (s1 s2 last) (cond
					 ((or (null? last) (eqv? last 'InvalidLExp)) last)
					 ((and (eqv? (car last) 'Bound-Var) (eqv? (cadr last) s1)) s2)
					 ((eqv? (car last) 'Abs) `(Abs ,(cadr last) ,(substitute s1 s2 (caddr last))))
					 ((eqv? (car last) 'App) `(App ,(substitute s1 s2 (cadr last)) ,(substitute s1 s2 (caddr last))))
					 ((eqv? (car last) 'Opr) `(Opr ,(cadr last) ,(substitute s1 s2 (caddr last)) ,(substitute s1 s2 (cadddr last))))
					 (else last))
			   ))

;;reduce-app :: Last -> Last -> Maybe Last
(define reduce-app (lambda (last1 last2) (cond
					  ((eqv? (car last1) 'Abs) (substitute (cadr last1) last2 (caddr last1)))
					  ((eqv? (car last1) 'App) (reduce-app (beta-reduce last1) last2))
					  (else 'InvalidLExp)
					  )))
;;beta-reduce :: Last -> Last
(define beta-reduce (lambda (last) (cond
				    ((null? last) last)
				    ((eqv? (car last) 'App) (reduce-app (cadr last) (caddr last)))
				    (else last)
				    )))

;;reduce :: Last -> Maybe Lexp
(define reduce (lambda (last) (cond
			       ((or (null? last) (eqv? last 'InvalidLExp)) last)
			       ((eqv? (car last) 'Num) (cadr last))
			       ((eqv? (car last) 'Abs) `(lambda ,(cadr last) ,(reduce (caddr last))))
			       ((eqv? (car last) 'App) (reduce (beta-reduce last)))
			       ((eqv? (car last) 'Opr) `(,(cadr last) ,(reduce (caddr last)) ,(reduce (cadddr last))))
			       (else last))
		       ))

;;eval :: Maybe Lexp -> Maybe Int
(define eval (lambda (exp) (cond
			    ((or (null? exp) (eqv? exp 'InvalidLExp)) exp)
			    ((number? exp) exp)
			    ((and (list? exp) (= (length exp) 3)) (let ((e1 (eval (cadr exp))) (e2 (eval (caddr exp))))
						    (if (not (and (number? e1) (number? e2))) 'Evaluation_Undefined
							(cond
							 ((eqv? (car exp) '+ ) (+ e1 e2))
							 ((eqv? (car exp) '* ) (* e1 e2))
							 ))))
			    (else exp)
			    )))

;;simplify :: Lexp -> Lexp
(define simplify (lambda (exp) (if (null? (cadr exp)) (caddr exp)
				   `(lambda ,(list (car (cadr exp))) ,(simplify `(lambda ,(cdr (cadr exp)) ,(caddr exp)))))))
;;getcurriedform :: Lexp -> Lexp
(define getcurriedform (lambda (lexp) (cond
				      ((list? lexp) (cond
						     ((and (= (length lexp) 3) (eqv? (car lexp) 'lambda)) (simplify lexp))
						     ((= (length lexp) 2) `( ,(getcurriedform (car lexp)) ,(getcurriedform (cadr lexp))))
						     (else lexp)))
				      (else lexp)
				      )))



;;basic interpreter
(define interpret (lambda (exp env) (eval (reduce (alpha-convert (lookup (alpha-convert (parse (getcurriedform exp))) env))))))

;;static environment
(define env '((a (lambda (y) (+ y b))) (b 4)))

;;sample expression's
(define e1 '(a b))
(define e4 '(lambda (x) (+ x (+ c (+ a a)))))
(define e2 '(lambda (x) (lambda (y) (lambda (x) (+ x (+ x y))))))
(define e3 '((lambda (x) (+ x (- x c))) 3) )
(define e5 '(a ((lambda (y) (+ b y)) b)))
(define e6 '(a ((lambda (y) (* y b)) b)))
(define e7 '((((lambda (x y z) (+ x (+ y z))) 2) b) 4))

;;to test sample expressions , execute the following :-
(define t1 (interpret e1 env))
(define t2 (interpret e2 env))
(define t3 (interpret e3 env))
(define t4 (interpret e4 env))
(define t5 (interpret e5 env))
(define t6 (interpret e6 env))
(define t7 (interpret e7 env))
